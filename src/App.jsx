import './App.css';
import Footer from './components/footer/footer';
import Navbar from './components/navbar/navbar';
import PriceChart from './components/priceChart/priceChart';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Profile from './components/profile/profile';
import Chart from './components/chart/chart';
import Knowledge from './components/knowledge/knowledge';

function App() {
  return (
    <Router>

      <div className="App">
        <Navbar />
        <Routes>
          <Route exact path="" element={<PriceChart/>}></Route>
          <Route path="/profil" element={<Profile/>}></Route>
          <Route path="/graf" element={<Chart/>}></Route>
          <Route path="/kunnskap" element={<Knowledge/>}></Route>
          <Route path="/" element={<PriceChart/>}></Route>
          <Route path="/hjem" element={<PriceChart/>}></Route>
        </Routes>
        <Footer></Footer>
      </div>


    </Router>
  );
}

export default App;
