const Navbar = () => {
    

    return (
        <nav>
            <ul className="navbar-list">
                <li>
                    Hjem
                </li>
                <li>
                    Kalkulator
                </li>
                <li>
                    Kontakt oss
                </li>
            </ul>
        </nav>
    )
    }

export default Navbar;