import Mwc from "../materialIcons/mwc";
import { Link } from "react-router-dom";

const Footer = () => {
    return (
        <footer>
            <ul className="footer-list">
                <li><Link to="/"><Mwc name="home"></Mwc></Link></li>
                <li><Link to="/graf"><Mwc name="show_chart"></Mwc></Link></li>
                <li><Link to="/kunnskap"><Mwc name="bolt"></Mwc></Link></li>
                <li><Link to="/profil"><Mwc name="person"></Mwc></Link></li>
            </ul>
        </footer>
    )
}

export default Footer;