import Mwc from "../materialIcons/mwc";

const Price = ({price, from, to}) => {

    const formatTime = (time) => time.toLocaleTimeString("en-GB", {hour: "2-digit", minute: "2-digit"});
    const priceInØre = Math.round((price * 100) * 10) /10;

    return (  
        <div className="price-circle">
            <h2 className="price">{priceInØre} øre</h2>
            <div className="time"><Mwc name="schedule"></Mwc> {formatTime(from)} - {formatTime(to)}</div>
        </div>  
        
    )
}

export default Price;