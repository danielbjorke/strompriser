import { useEffect, useState } from "react";
import { getPriceData } from "../../api/priceService";
import Price from "../priceChart/price"
import Mwc from "../materialIcons/mwc";

const PriceChart = () => {

    const [items, setItems] = useState([]);
    const [chosenDate, setChosenDate] = useState(new Date().toISOString().substring(0,10));
    const todaysDate = new Date();

    useEffect(() => {

        const loadData = async () => {
            try{
                setItems([]);
                let data = await getPriceData(chosenDate);
                Object.entries(data).forEach((i, index) => {
                    setItems( arr => [...arr, i[1]]);
                });
            }catch(error){
                console.log(error);
            }
        };

        loadData();

    }, [chosenDate])

    const dateFormatter = date => date.toISOString().substring(0, 10);

    const onDatePickerChange = event => {
        setChosenDate(event.target.value);
    }


    return(
            <div className="price-chart">
                <div className="date-header">
                    <h1>Strømpris</h1>
                    <span className="date-input">
                        <h1>Dato</h1>
                        <input 
                            onChange={onDatePickerChange}
                            type="date" 
                            id="start" 
                            name="price-date" 
                            value={chosenDate === undefined ? dateFormatter(todaysDate) : chosenDate} 
                            min="2018-01-01" 
                            max="2023-01-01">

                        </input>
                    </span>
                    <h4>Sjekk priser time for time</h4>
                </div>

                <div className="prices">
                    {items.length > 0 ? 
                        items.map((item, index) => 
                            <Price key={index} price={item['NOK_per_kWh']} from={new Date(item['valid_from'])} to={new Date(item['valid_to'])}></Price>) 
                            : <Mwc name="electrical_services"></Mwc>}
                </div>
            </div>
    )
};

export default PriceChart;