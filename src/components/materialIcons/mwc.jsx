const Mwc = ({name}) => {
    return (
        <span className="material-icons">{name}</span>
    )
}

export default Mwc;