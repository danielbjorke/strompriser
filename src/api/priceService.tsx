import axios from "axios";

const apiUrl: string = 'https://norway-power.ffail.win/?zone=NO1&date=';

export const getPriceData = async (date: string) => {
    return axios.get(`${apiUrl}${date}`)
        .then(response => response.data);
}
